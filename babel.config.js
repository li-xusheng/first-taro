// babel-preset-taro 更多选项和默认值：


// https://github.com/NervJS/taro/blob/next/packages/babel-preset-taro/README.md
module.exports = {
  presets: [
    ['taro', {
      framework: 'react',
      ts: false
    }]
  ],
  plugins: [
    [
      "import",
      {
        "libraryName": "@nutui/nutui-react",
        "libraryDirectory": "dist/esm",
        "style": true,
        "camel2DashComponentName": false
      },
      'nutui-react'
    ],
    [
      'import',
      {
        libraryName: 'react-vant',
        libraryDirectory: 'es',
        style: false,
      },
    ],
    [
      "import",
      {
        "libraryName": "@antmjs/vantui",
        "libraryDirectory": "es",
        "style": (name) => `${name}/style/less`,
      },
      "@antmjs/vantui"
    ]
  ],

}
