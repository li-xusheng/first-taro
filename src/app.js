
import { Component } from 'react'
import { Provider } from 'react-redux'
import 'taro-ui/dist/style/index.scss'
import "taro-ui/dist/style/components/form.scss";
import './app.scss'
import dva from './utils/dvaUtils';


const dvaApp = dva.createApp({

      initialState: {}
  
  });
  
const store = dvaApp.getStore();
  

class App extends Component {

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  render () {
    // this.props.children 是将要会渲染的页面
    return ( 
       <Provider store={store}>
          {this.props.children}
      </Provider>)
  }
}

export default App
