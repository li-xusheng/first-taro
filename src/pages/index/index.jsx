
import { connect } from "react-redux"
import { forwardRef} from 'react'
import { View, Input,Text } from '@tarojs/components'
import { Form, FormItem,Icon} from '@antmjs/vantui'
import './index.scss'


 const Index =forwardRef((props)=> {
  const formIt = Form.useForm()
  const {name} =props
    return (
      <View className='index'>
        <Text>----{name}</Text>
      <Form
        form={formIt}
      >
        <FormItem 
          label='用户名'
          name='userName'
          required
          rules={{
          rule: /[\u4e00-\u9fa5]/,
          message: '用户名仅支持中文',
        }}
          trigger='onInput'
          validateTrigger='onBlur'
        // taro的input的onInput事件返回对应表单的最终值为e.detail.value
          valueFormat={(e) => e.detail.value}
          renderRight={<Icon name='user-o' />}
        >
          <Input placeholder='请输入用户名（中文）' />
        </FormItem>
      </Form>
      </View>
    )
  })

 export default connect(state => ({...state.index}))(Index);

